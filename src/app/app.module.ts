import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';

// pages
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { SideBarComponent } from './pages/sideBar/side-bar.component';
import { TopComponent } from './pages/top/top.component';
import { PropsComponent } from './pages/props/props.component';
import { GeneratorsComponent } from './pages/generators/generators.component';
import { BottomComponent } from './pages/bottom/bottom.component';
import { SearchComponent } from './pages/search/search.component';
import { InfoBarComponent } from './pages/info-bar/info-bar.component';
import { DeleteDialogComponent } from './pages/dialogs/delete-dialog/delete-dialog.component';
import { DialComponent } from './components/dial/dial.component';
import { ShadowComponent } from './pages/generators/gens/shadow/shadow.component';
import { TransformComponent } from './pages/generators/gens/transform/transform.component';
import { ColorDialogComponent } from './pages/dialogs/color-dialog/color-dialog.component';

// directives-pipes
import { HighlightDirective } from './directives/highlight.drective';
import { FilterPipe } from './pipes/filter.pipe';

// misc
import { AngularSplitModule } from 'angular-split';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorSketchModule } from 'ngx-color/sketch';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SaveDialogComponent } from './pages/dialogs/save-dialog/save-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SideBarComponent,
    TopComponent,
    PropsComponent,
    GeneratorsComponent,
    BottomComponent,
    SearchComponent,
    HighlightDirective,
    FilterPipe,
    InfoBarComponent,
    DeleteDialogComponent,
    DialComponent,
    ShadowComponent,
    TransformComponent,
    ColorDialogComponent,
    SaveDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularSplitModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatTooltipModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatSliderModule,
    FormsModule,
    ReactiveFormsModule,
    ColorSketchModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
