import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IElements } from 'src/app/models/elements.model';
import { IGenerators } from 'src/app/models/generators.model';
import { IProperties } from 'src/app/models/properties.model';
import { IMyCustom } from "../models/mycustom.model";
import * as elementsData from '../data/elements.json';
import * as generatorsData from '../data/generators.json';
import * as propertiesData from '../data/properties.json';

@Injectable({
    providedIn: 'root'
})
export class GlobalValues {

  // data stores
  public elementsData: IElements[] = (elementsData as any).default;
  public generatorsData: IGenerators[] = (generatorsData as any).default;
  public propertiesData: IProperties[] = (propertiesData as any).default;
  
  // local storage
  //public myCustomData: IMyCustom[] = [];
  private myCustomData: BehaviorSubject<IMyCustom[]> = new BehaviorSubject<IMyCustom[]>([]);
  public myCustomDataStat = this.myCustomData.asObservable();
  public updatemyCustomDataStat(stat: IMyCustom[]) {
    this.myCustomData.next(stat);
  }

  //** observers ***//
  // left panel
  private leftPanelOpen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public leftPanelStat = this.leftPanelOpen.asObservable();
  public updateLeftPanelStat(stat: any) {
    this.leftPanelOpen.next(stat);
  }

  // right panel
  private rightPanelOpen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public rightPanelStat = this.rightPanelOpen.asObservable();
  public updateRightPanelStat(stat: any) {
    this.rightPanelOpen.next(stat);
  }

  // selected template type
  private selTemplateType: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public selTemplateTypeStat = this.selTemplateType.asObservable();
  public updateSelTemplateTypeStat(stat: any) {
    this.selTemplateType.next(stat);
  }

  // selected template id
  private selTemplateId: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public selTemplateIdStat = this.selTemplateId.asObservable();
  public updateSelTemplateIdStat(stat: any) {
    this.selTemplateId.next(stat);
  }

  // searchText
  private searchText: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public searchTextDat = this.searchText.asObservable();
  public updateSearchTextDat(dat: any) {
    this.searchText.next(dat);
  }

  // bread crumb
  private breadCrumb: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public breadCrumbDat = this.breadCrumb.asObservable();
  public updateBreadCrumbDat(dat: any) {
    this.breadCrumb.next(dat);
  }
 
  // properties accordion expand all
  private propAccordExpand: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public propAccordExpandStat = this.propAccordExpand.asObservable();
  public updatepropAccordExpandStat(stat: any) {
    this.propAccordExpand.next(stat);
  }

  // properties accordion collapse all
  private propAccordCollapse: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public propAccordCollapseStat = this.propAccordCollapse.asObservable();
  public updatepropAccordCollapseStat(stat: any) {
    this.propAccordCollapse.next(stat);
  }

  
  // generators observers and globals
  // color
  private genShadowColor: BehaviorSubject<string> = new BehaviorSubject<string>('#C9C9C9');
  public genShadowColorStat = this.genShadowColor.asObservable();
  public updategenShadowColorStat(stat: any) {
    this.genShadowColor.next(stat);
  }

  // angle dial control value
  private dialCtrl: BehaviorSubject<number> = new BehaviorSubject<number>(10);
  public dialCtrlStat = this.dialCtrl.asObservable();
  public updatedialCtrlStat(stat: any) {
    this.dialCtrl.next(stat);
  }

}