import { Injectable } from '@angular/core';
import { PropStoreService } from './propStore.service';
import { IMyCustom } from "../models/mycustom.model";
import { GlobalValues } from './globalValues.service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localStorage: Storage;

  constructor(
    protected propStoreService: PropStoreService,
    private globalValues: GlobalValues
  ) { 
    this.localStorage = window.localStorage;
  }

  get isLocalStorageSupported(): boolean {
    return !!this.localStorage
  }

  saveToStorage(key: string, dat: any) {
    //TODO cant have dupe names
    if (this.isLocalStorageSupported) {
      this.localStorage.setItem(key, JSON.stringify(dat));
      // read all again to update UI with new template
      this.getAllFromStorage();
    } else {
      console.log("not supported");
    }
  }

  getAllFromStorage() {
    let myCustomData: IMyCustom[] = [];
    let keyName: string = "";
    let keyVal: string = "";

    // check to see if local storage available and if something is in it
    if ((this.isLocalStorageSupported) && (this.localStorage.length > 0)) {
      for ( var i = 0, len = this.localStorage.length; i < len; ++i ) {
        keyName = this.localStorage.key(i)!;
        keyVal = this.localStorage.getItem(this.localStorage.key(i)!)!;
        // build array and return to be put in globalValues
        myCustomData.push({"name":keyName, "dat": keyVal});
      }
      // update observer with all saved templated
      this.globalValues.updatemyCustomDataStat(myCustomData);
    } else {
      // update observer with empty list of templates
      this.globalValues.updatemyCustomDataStat(myCustomData);
    }  
  }

  //https://firstclassjs.com/persist-data-using-local-storage-and-angular/
/*
  remove(key: string): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.removeItem(key);
      return true;
    }
    return false;
  }*/

}
