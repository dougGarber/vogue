import { Injectable } from "@angular/core";
import { GlobalValues } from '../services/globalValues.service';
import { IPropStore, IProps } from 'src/app/models/propStore.model';
import { UUID } from "angular2-uuid";
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PropStoreService {

  private propertyStore: BehaviorSubject<IPropStore[]> = new BehaviorSubject<IPropStore[]>([]);
  public propStore = this.propertyStore.asObservable();

  private _propStore: IPropStore[] = [];
  type:string = '';
  newHTMLTemplate: string = "";

  constructor(
    protected globalValues: GlobalValues
  ) { 
      // init template type observer
      this.globalValues.selTemplateTypeStat.subscribe(x =>
        this.type = x
      );
      // init template id observer
      this.globalValues.selTemplateIdStat.subscribe(x =>
        this.loadPropStore(Number(x))
      );
  }


  public loadPropStore(id: number) {
    let selTemplateData :any[] = [];
    let selTemplateDataKeys: any;
    let datProp: IProps[] = [];
    this._propStore.length = 0;

    switch(this.type) {
      // element template selected  
      case 'E': {
        this.globalValues.elementsData.forEach(el => {  
          if (el.id == id) {
            // get only element template  
            selTemplateData.push(el.cssTemplate);
            // get key(s)/classes of element
            selTemplateDataKeys = Object.keys(el.cssTemplate);

            // loop each key/class and get prop/values
            for (let key of selTemplateDataKeys) {
              for (let obj of selTemplateData) {
                for (let prop of obj[key] as any) {
                    //datProp += prop.prop + ":" + prop.val + ";"
                    datProp.push({prop:prop.prop,val:prop.val,visible:true});
                }    
              }
              //this._propStore.push({id:UUID.UUID(),class:key,props:datProp});
              //datProp = "";
              this._propStore.push({id:UUID.UUID(),htmlTemplate:el.htmlTemplate,class:key,open:true,props:datProp});
              datProp = [];
            }
          } 
        });
        // update the observer  
        this.propertyStore.next(Object.assign([], this._propStore));
        break;    
      }
      // generator template selected
      case 'G': {
        this.globalValues.generatorsData.forEach(el => {  
            if (el.id == id) {
              // get only element template  
              selTemplateData.push(el.cssTemplate);
              // get key(s)/classes of element
              selTemplateDataKeys = Object.keys(el.cssTemplate);
  
              // loop each key/class and get prop/values
              for (let key of selTemplateDataKeys) {
                for (let obj of selTemplateData) {
                  for (let prop of obj[key] as any) {
                      //datProp += prop.prop + ":" + prop.val + ";"
                      datProp.push({prop:prop.prop,val:prop.val,visible:true});
                  }    
                }
                //this._propStore.push({id:UUID.UUID(),class:key,props:datProp});
                //datProp = "";
                this._propStore.push({id:UUID.UUID(),htmlTemplate:el.htmlTemplate,class:key,open:true,props:datProp});
                datProp = [];
              }
            } 
          });
          // update the observer  
          this.propertyStore.next(Object.assign([], this._propStore));          
        break;    
      }
    }
  }

  // add to propstore
  public addPropStore(id: string) {
      // update props
      this._propStore.forEach(el => {
        if (el.id == id) {
          el.props.push({prop:'* New Property *',val:'* New Value *',visible:true});    
        }
      });
      // update the observer  
      this.propertyStore.next(Object.assign([], this._propStore));
      console.log("add= ");
      console.log(this._propStore);
  }

  // update propstore
  public updatePropStore(id: string, dat: IProps[]) {
      // update the propStore html template and props
      let htmlTemp: string = ""
      let clas: string = "";

      // update props
      this._propStore.forEach(el => {
        if (el.id == id) {
            clas = el.class
            el.props = dat;
        }
      });

      // update htmlTemplate for each class
      this._propStore.forEach(el => {
           el.htmlTemplate = this.htmlTempUpdate(clas, el.htmlTemplate, dat);
      });

       // update the observer  
       this.propertyStore.next(Object.assign([], this._propStore));
       console.log("update= ");
       console.log(this._propStore);

  }

  htmlTempUpdate(clas: string, orig: string, dat: IProps[]) : string {
    // parse the htmlTemplate and insert new inline style

    let newProps: string = "";
    

    // first part of htmlTemplate string  
    var searchIdx1A:string = "class=";
    var searchIdx1B:string = "'" + clas;
    var searchIdx1 = searchIdx1A + searchIdx1B;
    var searchIdx1Len = searchIdx1.length;
    var idx1 = orig.indexOf(searchIdx1);
    var firstPart = orig.substring(0,searchIdx1Len + idx1 + 2);
    var firstPartLen = firstPart.length;

    // second part - style tag being replaced
    var newSrchStr = orig.substring(firstPart.length, orig.length);
    var searchIdx2:string = "'>";
    var idx2 = newSrchStr.indexOf(searchIdx2);
    var secondPart = newSrchStr.substring(0 + 2,idx2);
    var secondPartLen = secondPart.length;

    // third part of htmlTemplate string
    var totalLen = orig.length;
    var thirdPart = orig.substring(firstPartLen + secondPartLen +4, totalLen)

    dat.forEach(item => {
        if (item.visible == true) 
        {
          newProps += item.prop + ":" + item.val + ";"
        }
    });
    
    return this.newHTMLTemplate = firstPart + "style='" + newProps + "'>" + thirdPart;
  }

  // delete propstore
  public deletePropStore(id: string, name: string) {
    // delete prop
    let clas: string = "";
    let datProp: IProps[] = [];

    this._propStore.forEach(el => {
      if (el.id == id) {
        clas = el.class;
        el.props.forEach((item, index) => {
          if (item.prop == name) {
            el.props.splice(index,1);
          } else {
            datProp.push({prop:item.prop,val:item.val, visible:item.visible});
          }
        });
      } 
    });

    // update htmlTemplate for each class
    this._propStore.forEach(el => {
      el.htmlTemplate = this.htmlTempUpdate(clas, el.htmlTemplate, datProp);
    });

    // update the observer  
    this.propertyStore.next(Object.assign([], this._propStore));
    console.log("delete= ");
    console.log(this._propStore);
  }

}
