export interface IGenerators {
    id: number;
    display: string;
    name: string;
    htmlTemplate: string;
    cssTemplate: string;
  }