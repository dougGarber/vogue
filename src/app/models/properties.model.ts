export interface IProperties {
    id: number;
    name: string;
    options: [string];
    descr: string;
    ex: string;
    compat: Array<{
      prop: string,
      val: string
    }>;
    compat2: Array<{
        prop: string,
        val: string
    }>;
    more: string;
  }
  