export interface IPropStore {
  id: any;
  htmlTemplate: string;
  class: string;
  open: boolean;
  props: IProps[];
}

export interface IProps {
    prop: string;
    val: string;
    visible: boolean;
  }
