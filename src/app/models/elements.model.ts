export interface IElements {
  id: number;
  display: string;
  name: string;
  htmlTemplate: string;
  //cssTemplate: ICssTemplate[];
  cssTemplate: Array<{
    prop: string,
    val: string
}>
}

export interface ICssTemplate {
  prop: string,
  val: string
}