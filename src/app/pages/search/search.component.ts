import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  options: any[] = [];
  searchText!: string;
  test: string = "DDDD";


  constructor(
    protected globalValues: GlobalValues
  ) {}

  ngOnInit(): void {
    //init observers
    this.globalValues.searchTextDat.subscribe(x =>
      this.searchText = x
    );

    this.globalValues.propertiesData.forEach(x => {
      this.options.push(x.name);
    });

  }

  propertyClick(dat: string) {
    this.globalValues.updateSearchTextDat(dat);
 }

 clearSearchBox() {
   this.searchText = '';
   this.globalValues.updateSearchTextDat('');
 }
}
