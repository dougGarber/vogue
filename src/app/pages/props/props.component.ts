import { Component, OnInit, ElementRef } from '@angular/core';
import { PropStoreService } from '../../services/propStore.service';
import { IPropStore, IProps } from 'src/app/models/propStore.model';
import { GlobalValues } from '../../services/globalValues.service';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../dialogs/delete-dialog/delete-dialog.component';

@Component({
  selector: 'props',
  templateUrl: './props.component.html',
  styleUrls: ['./props.component.scss']
})
export class PropsComponent implements OnInit {
  public propStore: IPropStore[] = [];
  dialogRef!: MatDialogRef<DeleteDialogComponent>;
  //panelOpenState = true;
  openCB = "{";
  closeCB = "}";

  constructor(
    protected globalValues: GlobalValues,
    protected propStoreService: PropStoreService,
    private element: ElementRef,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    // init propStore observer
    this.propStoreService.propStore.subscribe(x => 
      this.propStore = x
      // for debug
      //this.buildProps(x)
    ); 


    this.globalValues.propAccordCollapseStat.subscribe(x =>
      this.propertyAccordionCollapseAll()
    );

    this.globalValues.propAccordExpandStat.subscribe(x =>
      this.propertyAccordionExpandAll()
    );


  }

  propAdd(id: string) {
    this.propStoreService.addPropStore(id);
  }

  propChange(id: string) {
    let els: any[] = [];
    let propVis: boolean;
    let propName: string = "";
    let propVal: string = "";
    let datProp: IProps[] = [];

     els = this.element.nativeElement.querySelectorAll('.propLine');
    
     // for the class/uuid get all of the prop/vals  
    els.forEach((item: any) => {
      if (item.id == id) {
        //table|tr|td|cbox|label|span|input
        propVis = item.children[0].children[0].children[0].children[0].children[0].children[0].children[0].checked;
        //table|tr|td|input|
        propName = item.children[0].children[0].children[1].children[0].value;
        propVal = item.children[0].children[0].children[3].children[0].value;

        datProp.push({prop:propName,val:propVal, visible:propVis});
      }
    });
    // update propStore
    this.propStoreService.updatePropStore(id,datProp);
  }

  updateSearch(dat: string) {
    this.globalValues.updateSearchTextDat(dat);
  }

  propertyAccordionExpandAll() {
    this.propStore.forEach(x => {
      x.open = true;
    });
  }

  propertyAccordionCollapseAll() {
    this.propStore.forEach(x => {
      x.open = false;
    });
  }

  openCloseProp(id: any) {
    this.propStore.forEach(x => {
      if(x.id == id) {
        if(x.open == true) {
          x.open = false;
        } else {
          x.open = true;
        }
      }
    });
  }

  openDeleteDialog(id: string, name: string) {
    this.dialogRef = this.dialog.open(DeleteDialogComponent, {
      disableClose: true
    });
    this.dialogRef.componentInstance.confirmMessage = "Are you sure you want to delete property '" + name + "' ?"

    this.dialogRef.afterClosed().subscribe(result => {
      if(result) {
        // do confirmation actions
        this.propStoreService.deletePropStore(id, name);
      }
    });
  }

  // for debug
  buildProps(dat: IPropStore[]) {
    this.propStore = dat;
    console.log(dat);
  }

}
