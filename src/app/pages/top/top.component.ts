import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';

@Component({
  selector: 'top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

  public templateType: any;
  public showComponent: string = 'props';
  
  constructor(
    protected globalValues: GlobalValues
  ) {}

  ngOnInit(): void {
    // init observer
    this.globalValues.selTemplateTypeStat.subscribe(x =>
      this.templateType = x
    );
  }

  setComponent(dat: string) {
    this.showComponent = dat;
  }

}
