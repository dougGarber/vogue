import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { MatDialog } from '@angular/material/dialog';
import { SaveDialogComponent } from '../dialogs/save-dialog/save-dialog.component';
import { PropStoreService } from '../../services/propStore.service';
import { IPropStore } from 'src/app/models/propStore.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public propStore: IPropStore[] = [];
  public leftPanelStatus: any;
  public rightPanelStatus: any;
  public breadCrumb: string = '';
  cssIco="{ ; }";

  constructor(
    protected propStoreService: PropStoreService,
    protected globalValues: GlobalValues,
    private storageService: LocalStorageService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {

    //init observers
    this.globalValues.leftPanelStat.subscribe(x =>
      this.leftPanelStatus = x
    );

    this.globalValues.rightPanelStat.subscribe(x =>
      this.rightPanelStatus = x
    );

    this.globalValues.breadCrumbDat.subscribe(x => 
      this.breadCrumb = x
    );

    this.propStoreService.propStore.subscribe(x => 
      this.propStore = x
    ); 
  }

  ToggleLeftSide() {
    // open and close sidebar
    if (this.leftPanelStatus == true) {
      this.globalValues.updateLeftPanelStat(false);
    } else {
      this.globalValues.updateLeftPanelStat(true);
    }
  }

  ToggleRightSide() {
    // open and close infobar
    if (this.rightPanelStatus == true) {
      this.globalValues.updateRightPanelStat(false);
    } else {
      this.globalValues.updateRightPanelStat(true);
    }
  }

  expandAll_click() {
    // expanding all properties accordion list
    this.globalValues.updatepropAccordExpandStat(true);
  }

  collapseAll_click() {
    // collapsing all properties accordion list
    this.globalValues.updatepropAccordCollapseStat(true);
  }

  saveToMyCustom_click() {
    // save to mycustom
    const dialogRef = this.dialog.open(SaveDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != "cancel") {
        this.storageService.saveToStorage(result,this.propStore);
      }
    });
  }

}
