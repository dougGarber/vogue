import { Component, ElementRef, OnInit, Renderer2, ViewChild, ChangeDetectorRef } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';
import { PropStoreService } from '../../services/propStore.service';
import { IPropStore } from 'src/app/models/propStore.model';

@Component({
  selector: 'bottom',
  templateUrl: './bottom.component.html',
  styleUrls: ['./bottom.component.scss']
})
export class BottomComponent implements OnInit {
  @ViewChild('bottomContainer', { static: false }) container?: ElementRef;
  type:string = '';

  constructor(
    protected propStoreService: PropStoreService,
    private renderer: Renderer2,
    private changeDetector: ChangeDetectorRef
    //protected globalValues: GlobalValues
  ) { 
      // init type observer earlier than id
     // this.globalValues.selTemplateTypeStat.subscribe(x =>
     //   this.type = x
     // );
  }

  ngOnInit(): void {
    //init id observer
   // this.globalValues.selTemplateIdStat.subscribe(x =>
   //   this.updateContainer(Number(x))
   // );
   this.propStoreService.propStore.subscribe(x => 
     this.updateContainer(x)
   );
  }

  updateContainer(dat: IPropStore[]) {
    // all of the htmlTemplates are the same so grab first one and break
    for (var i=0; i<dat.length; i++) {
      this.renderer.setProperty(this.container?.nativeElement,'innerHTML', dat[i].htmlTemplate);
      //this.changeDetector.detectChanges();
      break;
    }
  }

  /*
  updateContainer(id: number) {

    // determine which type and then gather data from store
    switch(this.type) {
      case 'E': {
        this.globalValues.elementsData.forEach(el => {
          if (el.id == id) {
            // append to container
            this.renderer.setProperty(this.container?.nativeElement,'innerHTML', el.htmlTemplate);
          }
        });
        break;
      }
      case 'G': {
        this.globalValues.generatorsData.forEach(gn => {
          if (gn.id == id) {
            // append to container
            this.renderer.setProperty(this.container?.nativeElement,'innerHTML', gn.htmlTemplate);
          }
        });
        break;
      }
    }

  }
*/
}
