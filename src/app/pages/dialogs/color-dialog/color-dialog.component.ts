import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ColorEvent } from 'ngx-color';
import { GlobalValues } from '../../../services/globalValues.service';

@Component({
  selector: 'colorDialog',
  templateUrl: './color-dialog.component.html',
  styleUrls: ['./color-dialog.component.scss']
})
export class ColorDialogComponent {
  public confirmMessage:string = "";
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public dialogRef: MatDialogRef<ColorDialogComponent>,
    protected globalValues: GlobalValues
    ) {}

  // not being used  
  onNoClick(): void {
    this.dialogRef.close(true);
  }

  changeComplete($event: ColorEvent) {
    console.log($event.color);
    this.globalValues.updategenShadowColorStat($event.color);
  }

}
