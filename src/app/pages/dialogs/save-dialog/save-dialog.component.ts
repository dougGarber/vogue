import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalValues } from 'src/app/services/globalValues.service';

@Component({
  selector: 'save-dialog',
  templateUrl: './save-dialog.component.html',
  styleUrls: ['./save-dialog.component.scss']
})
export class SaveDialogComponent {

  public errMessage: String = ""
  public myCustomData: any[] = [];

  constructor(
    private globalValues: GlobalValues,
    public dialogRef: MatDialogRef<SaveDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) 
    {
      // only close modal dialog with button
      dialogRef.disableClose = true;

      this.globalValues.myCustomDataStat.subscribe(x => 
        this.myCustomData = x
      ); 
    }

  onNoClick(): void {
    this.data = "cancel";
    this.dialogRef.close(this.data);
  }

  validateEntry() {
    // if saving then make sure name is not left blank
    if ((this.data == null) || (this.data == "")) {
      this.errMessage = "( Entry is blank! )";
    } else {
      // make sure name does not already exist
      if (this.checkForDuplicateName()) {
        this.errMessage = "( Template name already exists! )";
      } else {
        // close dialog and return name value
        this.dialogRef.close(this.data);
      }
    }
  }

  checkForDuplicateName(): Boolean {
    for (var i=0; i < this.myCustomData.length; i++) {
      if (this.myCustomData[i].name == this.data) {
        this.data = "";
        // found a duplicate name
        return true;
      }
    }
    // no duplicates found
    return false;
  }

}
