import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'deleteDialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent {
  public confirmMessage:string = "";
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public dialogRef: MatDialogRef<DeleteDialogComponent>) 
    {
      // only close modal dialog with button
      dialogRef.disableClose = true;
    }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }

}
