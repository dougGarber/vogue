import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';

@Component({
  selector: 'generators',
  templateUrl: './generators.component.html',
  styleUrls: ['./generators.component.scss']
})
export class GeneratorsComponent implements OnInit {

  templateId!: number;

  constructor(
    protected globalValues: GlobalValues
  ) { }

  ngOnInit(): void {
    // init template id observer
    this.globalValues.selTemplateIdStat.subscribe(x =>
      this.templateId=(Number(x))
    );
  }

}
