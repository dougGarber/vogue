import { Component, OnDestroy, OnInit } from '@angular/core';
import { ColorDialogComponent } from '../../../dialogs/color-dialog/color-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { GlobalValues } from '../../../../services/globalValues.service';
import { PropStoreService } from '../../../../services/propStore.service';
import { IPropStore, IProps } from 'src/app/models/propStore.model';

@Component({
  selector: 'shadow',
  templateUrl: './shadow.component.html',
  styleUrls: ['./shadow.component.scss']
})
export class ShadowComponent implements OnInit, OnDestroy {
  propStore: IPropStore[] = [];
  templateId!: any;
  dialogRef!: MatDialogRef<ColorDialogComponent>;

  colorValue: string = "";
  distMax: number = 75;
  distMin: number = -75;
  distValue: number = 30;
  blurMax: number = 30;
  blurMin: number = 0;
  blurValue: number = 5;
  spreadMax: number = 50;
  spreadMin: number = -50;
  spreadValue: number = 0;
  angleValue: number = 10.0;

  constructor(
    public dialog: MatDialog,
    protected globalValues: GlobalValues,
    protected propStoreService: PropStoreService
  ) { }

  ngOnInit(): void {
    //init observers
    this.globalValues.genShadowColorStat.subscribe(x =>
      this.getColor(x)
    );
    this.globalValues.dialCtrlStat.subscribe(x => {
      this.angleValue = Number(x);
      this.propChange();
    });
    this.globalValues.selTemplateIdStat.subscribe(x =>
      this.templateId = x
    );
    this.propStoreService.propStore.subscribe(x => 
      this.propStore = x
    ); 
  }

  ngOnDestroy(): void {
    // reset the color on exit
    this.globalValues.updategenShadowColorStat('');
  }

  openColorDialog() {
    this.dialogRef = this.dialog.open(ColorDialogComponent, {
      height: '400px',
      width: '250px',
      panelClass: 'colorDialogCont'
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if(result) {
      }
    });
  }

  getColor(dat: any) {
    // json string of color in different formats. using HEX.
    if (dat == '') {
      this.colorValue = "#C9C9C9";
    } else {
      this.colorValue = dat.hex;
      this.propChange();
    }
  }

  propChange() {
    let id: string;
    let datProp: IProps[] = [];

    // each generator has different props that are affected
    // by the generator controls. This switch targets those
    // specific props per the generator being used
    switch(this.templateId) {
      case 0: {
        // modifying box-shadow property
        //box-shadow:inset 7px 7px 5px 0px rgba(50, 50, 50, 0.75);
        // inset option   x,y,blur,spread               opacity

        // shadow selected - property being modified box-shadow
        id = this.idSearch('box-shadow');
        datProp = this.setProps(id,'box-shadow');
        // update propStore
        this.propStoreService.updatePropStore(id,datProp);
        break;
      }
      case 1: {
        // transform selected - property being modified transform
        id = this.idSearch('transform');
        datProp = this.setProps(id,'transform');
        // update propStore
        this.propStoreService.updatePropStore(id,datProp);
        break;
      }
    }
  }

  idSearch(propName: string): any {
    // get the uuid for the class that has the property that is being updated
    for (var i=0; i<this.propStore.length; i++) {
      for(var j=0; j<this.propStore[i].props.length; j++) {
        if(this.propStore[i].props[j].prop == propName) {
          return this.propStore[i].id;
          break;
        }
      }  
    }
  }

  setProps(id: string, propName: string): IProps[] {
    let tempArr: IProps[] = [];
    let propNm: string = "";
    let propVal: string = "";
    let propVis: boolean;

    // iterate the propStore and get its prop array and rebuild it
    for (var i=0; i<this.propStore.length; i++) {
      if (this.propStore[i].id == id) {
        for(var j=0; j<this.propStore[i].props.length; j++) {
          if(this.propStore[i].props[j].prop == propName) {
            propNm = this.propStore[i].props[j].prop;
            propVal = this.getAngleX() + "px " + this.getAngleY() + "px " + this.blurValue + "px " + this.spreadValue + "px " + this.colorValue;
            propVis = this.propStore[i].props[j].visible;
            tempArr.push({prop:propNm,val:propVal,visible:propVis});
          } else {
            propNm = this.propStore[i].props[j].prop;
            propVal = this.propStore[i].props[j].val;
            propVis = this.propStore[i].props[j].visible;
            tempArr.push({prop:propNm,val:propVal,visible:propVis});
          }
        }
      }
      
    }
    return tempArr;
  }

  // get the appropriate x axis based on distance and angle(dial)
  getAngleX(): string {
    let radians = this.angleValue * Math.PI / 180;
    let xDistance = Math.cos(radians) * this.distValue;
    return String(xDistance.toFixed(2));
    
  }

  // get the appropriate y axis based on distance and angle(dial)
  getAngleY(): string {
    let radians = this.angleValue * Math.PI / 180
    let yDistance = Math.sin(radians) * this.distValue;
    return String(yDistance.toFixed(2));
  }
}
