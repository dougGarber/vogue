import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  public elementsData = this.globalValues.elementsData;
  public generatorsData = this.globalValues.generatorsData;
  public myCustomData: any[] = [];

  constructor(
    protected globalValues: GlobalValues,
    private storage: LocalStorageService,
  ) { }

  ngOnInit(): void {
  // this.globalValues.myCustomData = this.storage.getAllFromStorage();
  //  this.myCustomData = this.globalValues.myCustomData;
  this.storage.getAllFromStorage();
  this.globalValues.myCustomDataStat.subscribe(x => 
    this.myCustomData = x
  ); 
  }

  element_OnClick(type:string, id:any) {
    // update the selected template type and id observers
    this.globalValues.updateSelTemplateTypeStat(type);
    this.globalValues.updateSelTemplateIdStat(id);

    this.updateBreadCrumb(type,id);
  }

  updateList() {
    // refreshes the mycustom list when accordion title is clicked
    // if someone were to manually delete from storage the ui would be
    // updated by open close the accordion
    this.storage.getAllFromStorage();
  }

  updateBreadCrumb(type:string, id:any) {
    let cat: string = '';
    let template: any;
    let holder;

    // get the pieces needed to form the breadcrumb
    switch(type) {
      case 'E' :{
        cat = "Elements";
        holder = this.globalValues.elementsData.find(item => item.id === id);
        template = holder?.display;
        break;
      }
      case 'G' :{
        cat = "Generators";
        holder = this.globalValues.generatorsData.find(item => item.id === id);
        template = holder?.display;
        break;
      }
    }

    // update the breadcrumb observer
    this.globalValues.updateBreadCrumbDat(cat + ' > ' + template);

    // update searchtext observer - reset
    this.globalValues.updateSearchTextDat("");
  }
}
