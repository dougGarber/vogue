import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';
import { IProperties } from '../../models/properties.model';
import { isNull } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'infoBar',
  templateUrl: './info-bar.component.html',
  styleUrls: ['./info-bar.component.scss']
})
export class InfoBarComponent implements OnInit {
  public templateType: any;
  selectedProp = {} as IProperties;
  propFound: boolean = false;

  constructor(
    protected globalValues: GlobalValues
  ) { }

  ngOnInit(): void {
    // init observer
    this.globalValues.selTemplateTypeStat.subscribe(x =>
      this.templateType = x
    );

    this.globalValues.searchTextDat.subscribe(x =>
      this.propSelect(x)
    );
  }

  propSelect(dat: string) {
    if (dat != '') {
    for(var i = 0; i < this.globalValues.propertiesData.length; i++) {
      if(this.globalValues.propertiesData[i].name == dat) {
        this.selectedProp = this.globalValues.propertiesData[i];
        this.selectedPropIcon();
        this.propFound = true;
        break;
      } else {
        this.propFound = false;
      }
    };
  } else {
    // empty infobar if searchtext is empty
    this.selectedProp = {} as IProperties;
  }

  // empty info bar if property is not found
  if(!this.propFound) {
    this.selectedProp = {} as IProperties;
  }

  }

  selectedPropIcon() {
    // set icon image for compat
    this.selectedProp.compat.forEach(x => {
      switch(x.prop) {
        case "ff": {
          x.prop = "icoFF.png";
          break;
        }
        case "gc": {
          x.prop = "icoChrome.png";
          break;
        }
        case "ie": {
          x.prop = "icoIE.png";
          break;
        }
        case "s": {
          x.prop = "icoSafari.png";
          break;
        }
        case "o": {
          x.prop = "icoOpera.png";
          break;
        }
      }
    });

    // set icon image for compat2
    this.selectedProp.compat2.forEach(x => {
      switch(x.prop) {
        case "aw": {
          x.prop = "icoAndroid.png";
          break;
        }
        case "agc": {
          x.prop = "icoChrome.png";
          break;
        }
        case "aff": {
          x.prop = "icoFF.png";
          break;
        }
        case "ao": {
          x.prop = "icoOpera.png";
          break;
        }
        case "is": {
          x.prop = "icoSafari.png";
          break;
        }
      }
    });

  }

}
