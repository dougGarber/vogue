import { Component, OnInit } from '@angular/core';
import { GlobalValues } from '../../services/globalValues.service';

declare let $: any;  
@Component({
    selector: 'dial',
    templateUrl: './dial.component.html',
    styleUrls: ['./dial.component.scss']
  })
  
  export class DialComponent implements OnInit {
    
    constructor(
      protected globalValues: GlobalValues
    ) { }

    ngOnInit(): void {
        $("#slider").roundSlider({
            min: 0,
            max: 360,
            width: 15,
            radius: 55,
            sliderType: "min-range",
            value: 50,
            change: (e: any) => {
                this.dialChange(e);
            }
        });
    }

    dialChange(e: any) {
      // update observer with angle value
      this.globalValues.updatedialCtrlStat(e.value);
    }
  }

